<?php

namespace App\Http\Controllers;

use App\Models\Arquivo;
use App\Models\FeedbackCurriculo;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class DossieController extends Controller
{

    public function show(Request $request,  $feedback)
    {
        $feedback_id = $feedback;

        $feedback = FeedbackCurriculo::select('id','curriculo_id')->whereId($feedback_id)->with(
            'DocSelecao',
            'DocChecklist',
            'FichaRegistrada',
            'ContratoTrabalhoAssinado',
            'TermoConfiabilidade',
            'ValeTransporteAssinado',
            'AcordoHora',
            'SalarioFamiliaAssinado',
            'DeclaracaoDependentesImposto',
            'ComprovanteDevCtp',
            'OrdemServicoAssinada',
            'CertificadoTreinSeg',
            'FichaEntregaEpi',
            'ContraChequeMensais',
            'CartoesPonto',
            'AvisoFerias',
            'ControleAsos',
            'BookRescisao',
            'TermoRescisao',
            'GuiaSeguroDesemprego',
            'ChaveFgts',
            'ComprovantePagamento',
            'ExameDemissional',
            'NadaConstaFichaEpi',
            'ComprovanteDevolucaoCtps',
            'PppAssinado',
            'ArquivamentoEletronico',
            'ArquivamentoDossie'
        )->first();

        $feedback->doc_selecaoDel = [];
        $feedback->doc_checklistDel = [];
        $feedback->ficha_registradaDel = [];
        $feedback->contrato_trabalho_assinadoDel = [];
        $feedback->termo_confiabilidadeDel = [];
        $feedback->vale_transporte_assinadoDel = [];
        $feedback->acordo_horaDel = [];
        $feedback->salario_familia_assinadoDel = [];
        $feedback->declaracao_dependentes_impostoDel = [];
        $feedback->comprovante_dev_ctpDel = [];
        $feedback->ordem_servico_assinadaDel = [];
        $feedback->certificado_trein_segDel = [];
        $feedback->ficha_entrega_epiDel = [];
        $feedback->contra_cheque_mensaisDel = [];
        $feedback->cartoes_pontoDel = [];
        $feedback->aviso_feriasDel = [];
        $feedback->controle_asosDel = [];
        $feedback->book_rescisaoDel = [];
        $feedback->termo_rescisaoDel = [];
        $feedback->guia_seguro_desempregoDel = [];
        $feedback->chave_fgtsDel = [];
        $feedback->comprovante_pagamentoDel = [];
        $feedback->exame_demissionalDel = [];
        $feedback->nada_consta_ficha_epiDel = [];
        $feedback->comprovante_devolucao_ctpsDel = [];
        $feedback->ppp_assinadoDel = [];
        $feedback->arquivamento_eletronicoDel = [];
        $feedback->arquivamento_dossieDel = [];

        return $feedback;
    }

    public function store(Request $request,  $feedback)
    {

        $feedback = FeedbackCurriculo::whereId($feedback)->first();

        $dados = $request->input();
        $dadosValidados = \Validator::make($dados, [

        ]);

        if ($dadosValidados->fails()) { // se o array de erros contem 1 ou mais erros..
            return response()->json([
                'msg' => 'Erro ao Salvar Informações',
                'erros' => $dadosValidados->errors()
            ], 400);
        } else {
            try {
                DB::beginTransaction();

                //Remove DocSelecao
                if (isset($dados['doc_selecaoDel'])) {
                    foreach ($dados['doc_selecaoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova DocSelecao
                if (isset($dados['doc_selecao'])) {
                    foreach ($dados['doc_selecao'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();

                            $feedback->DocSelecao()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'DocSelecao',
                                'label' => 'DOCUMENTO DE SELEÇÃO'
                            ]);
                        }
                    }
                }

                //Remove DocChecklist
                if (isset($dados['doc_checklistDel'])) {
                    foreach ($dados['doc_checklistDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova DocChecklist
                if (isset($dados['doc_checklist'])) {
                    foreach ($dados['doc_checklist'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->DocChecklist()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'DocChecklist',
                                'label' => 'DOCUMENTOS CHECK LIST ADMISSÃO'
                            ]);
                        }
                    }
                }

                //Remove FichaRegistrada
                if (isset($dados['ficha_registradaDel'])) {
                    foreach ($dados['ficha_registradaDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova DocSelecao
                if (isset($dados['ficha_registrada'])) {
                    foreach ($dados['ficha_registrada'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->FichaRegistrada()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'FichaRegistrada',
                                'label' => 'FICHA REGISTRO ASSINADA'
                            ]);
                        }
                    }
                }

                //Remove ContratoTrabalhoAssinado
                if (isset($dados['contrato_trabalho_assinadoDel'])) {
                    foreach ($dados['contrato_trabalho_assinadoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ContratoTrabalhoAssinado
                if (isset($dados['contrato_trabalho_assinado'])) {
                    foreach ($dados['contrato_trabalho_assinado'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ContratoTrabalhoAssinado()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ContratoTrabalhoAssinado',
                                'label' => 'CONTRATO DE TRABALHO ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove TermoConfiabilidade
                if (isset($dados['termo_confiabilidadeDel'])) {
                    foreach ($dados['termo_confiabilidadeDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova TermoConfiabilidade
                if (isset($dados['termo_confiabilidade'])) {
                    foreach ($dados['termo_confiabilidade'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->TermoConfiabilidade()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'TermoConfiabilidade',
                                'label' => 'TERMO DE CONFIDENCIALIDADE ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove ValeTransporteAssinado
                if (isset($dados['vale_transporte_assinadoDel'])) {
                    foreach ($dados['vale_transporte_assinadoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ValeTransporteAssinado
                if (isset($dados['vale_transporte_assinado'])) {
                    foreach ($dados['vale_transporte_assinado'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ValeTransporteAssinado()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ValeTransporteAssinado',
                                'label' => 'OPÇÃO VALE TRANSPORTE ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove AcordoHora
                if (isset($dados['acordo_horaDel'])) {
                    foreach ($dados['acordo_horaDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova AcordoHora
                if (isset($dados['acordo_hora'])) {
                    foreach ($dados['acordo_hora'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->AcordoHora()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'AcordoHora',
                                'label' => 'ACORDO COMPENSAÇÃO DE HORAS ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove SalarioFamiliaAssinado
                if (isset($dados['salario_familia_assinadoDel'])) {
                    foreach ($dados['salario_familia_assinadoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova SalarioFamiliaAssinado
                if (isset($dados['salario_familia_assinado'])) {
                    foreach ($dados['salario_familia_assinado'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->SalarioFamiliaAssinado()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'SalarioFamiliaAssinado',
                                'label' => 'TERMO SALÁRIO FAMILIA ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove DeclaracaoDependentesImposto
                if (isset($dados['declaracao_dependentes_impostoDel'])) {
                    foreach ($dados['declaracao_dependentes_impostoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova DeclaracaoDependentesImposto
                if (isset($dados['declaracao_dependentes_imposto'])) {
                    foreach ($dados['declaracao_dependentes_imposto'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->DeclaracaoDependentesImposto()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'DeclaracaoDependentesImposto',
                                'label' => 'DECLARAÇÃO DEPENDENTES IMPOSTO DE RENDA ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove ComprovanteDevCtp
                if (isset($dados['comprovante_dev_ctpDel'])) {
                    foreach ($dados['comprovante_dev_ctpDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ComprovanteDevCtp
                if (isset($dados['comprovante_dev_ctp'])) {
                    foreach ($dados['comprovante_dev_ctp'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ComprovanteDevCtp()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ComprovanteDevCtp',
                                'label' => 'COMPROVANTE DEVOLUÇÃO CTPS ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove OrdemServicoAssinada
                if (isset($dados['ordem_servico_assinadaDel'])) {
                    foreach ($dados['ordem_servico_assinadaDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova OrdemServicoAssinada
                if (isset($dados['ordem_servico_assinada'])) {
                    foreach ($dados['ordem_servico_assinada'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->OrdemServicoAssinada()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'OrdemServicoAssinada',
                                'label' => 'ORDEM DE SERVIÇO ASSINADA'
                            ]);
                        }
                    }
                }

                //Remove CertificadoTreinSeg
                if (isset($dados['certificado_trein_segDel'])) {
                    foreach ($dados['certificado_trein_segDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova CertificadoTreinSeg
                if (isset($dados['certificado_trein_seg'])) {
                    foreach ($dados['certificado_trein_seg'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->CertificadoTreinSeg()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'CertificadoTreinSeg',
                                'label' => 'CERTIFICADOS DE TREINAMENTOS SEGURANÇA'
                            ]);
                        }
                    }
                }

                //Remove FichaEntregaEpi
                if (isset($dados['ficha_entrega_epiDel'])) {
                    foreach ($dados['ficha_entrega_epiDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova FichaEntregaEpi
                if (isset($dados['ficha_entrega_epi'])) {
                    foreach ($dados['ficha_entrega_epi'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->FichaEntregaEpi()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'FichaEntregaEpi',
                                'label' => 'FICHA DE ENTREGA DE EPI'
                            ]);
                        }
                    }
                }

                //Remove ContraChequeMensais
                if (isset($dados['contra_cheque_mensaisDel'])) {
                    foreach ($dados['contra_cheque_mensaisDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ContraChequeMensais
                if (isset($dados['contra_cheque_mensais'])) {
                    foreach ($dados['contra_cheque_mensais'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ContraChequeMensais()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ContraChequeMensais',
                                'label' => 'CONTRACHEQUES MENSAIS'
                            ]);
                        }
                    }
                }

                //Remove CartoesPonto
                if (isset($dados['cartoes_pontoDel'])) {
                    foreach ($dados['cartoes_pontoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova CartoesPonto
                if (isset($dados['cartoes_ponto'])) {
                    foreach ($dados['cartoes_ponto'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->CartoesPonto()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'CartoesPonto',
                                'label' => 'CARTÕES DE PONTO MENSAIS'
                            ]);
                        }
                    }
                }

                //Remove AvisoFerias
                if (isset($dados['aviso_feriasDel'])) {
                    foreach ($dados['aviso_feriasDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova AvisoFerias
                if (isset($dados['aviso_ferias'])) {
                    foreach ($dados['aviso_ferias'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->AvisoFerias()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'AvisoFerias',
                                'label' => 'AVISOS DE FÉRIAS ANUAIS'
                            ]);
                        }
                    }
                }

                //Remove ControleAsos
                if (isset($dados['controle_asosDel'])) {
                    foreach ($dados['controle_asosDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ControleAsos
                if (isset($dados['controle_asos'])) {
                    foreach ($dados['controle_asos'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ControleAsos()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ControleAsos',
                                'label' => 'PASTA DE CONTROLE DE ASOS: ADMISSIONAIS, PERIODICOS,MUDANÇA DE FUNÇÃO, DEMISSIONAL'
                            ]);
                        }
                    }
                }

                //Remove BookRescisao
                if (isset($dados['book_rescisaoDel'])) {
                    foreach ($dados['book_rescisaoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova BookRescisao
                if (isset($dados['book_rescisao'])) {
                    foreach ($dados['book_rescisao'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->BookRescisao()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'BookRescisao',
                                'label' => 'BOOK DE RESCISÃO – CHECK LIST DEMISSÃO'
                            ]);
                        }
                    }
                }

                //Remove TermoRescisao
                if (isset($dados['termo_rescisaoDel'])) {
                    foreach ($dados['termo_rescisaoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova TermoRescisao
                if (isset($dados['termo_rescisao'])) {
                    foreach ($dados['termo_rescisao'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->TermoRescisao()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'TermoRescisao',
                                'label' => 'TERMO DE RESCISAO DE CONTRATO DE TRABALHO ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove GuiaSeguroDesemprego
                if (isset($dados['guia_seguro_desempregoDel'])) {
                    foreach ($dados['guia_seguro_desempregoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova GuiaSeguroDesemprego
                if (isset($dados['guia_seguro_desemprego'])) {
                    foreach ($dados['guia_seguro_desemprego'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->GuiaSeguroDesemprego()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'GuiaSeguroDesemprego',
                                'label' => 'GUIAS DE SEGURO DESEMPREGO ASSINADAS'
                            ]);
                        }
                    }
                }

                //Remove ChaveFgts
                if (isset($dados['chave_fgtsDel'])) {
                    foreach ($dados['chave_fgtsDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ChaveFgts
                if (isset($dados['chave_fgts'])) {
                    foreach ($dados['chave_fgts'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ChaveFgts()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ChaveFgts',
                                'label' => 'CHAVE DE FGTS ASSINADA'
                            ]);
                        }
                    }
                }

                //Remove ComprovantePagamento
                if (isset($dados['comprovante_pagamentoDel'])) {
                    foreach ($dados['comprovante_pagamentoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ComprovantePagamento
                if (isset($dados['comprovante_pagamento'])) {
                    foreach ($dados['comprovante_pagamento'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ComprovantePagamento()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ComprovantePagamento',
                                'label' => 'COMPROVANTE DE PAGAMENTO'
                            ]);
                        }
                    }
                }

                //Remove ExameDemissional
                if (isset($dados['exame_demissionalDel'])) {
                    foreach ($dados['exame_demissionalDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ExameDemissional
                if (isset($dados['exame_demissional'])) {
                    foreach ($dados['exame_demissional'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ExameDemissional()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ExameDemissional',
                                'label' => 'EXAME DEMISSIONAL'
                            ]);
                        }
                    }
                }

                //Remove NadaConstaFichaEpi
                if (isset($dados['nada_consta_ficha_epiDel'])) {
                    foreach ($dados['nada_consta_ficha_epiDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova NadaConstaFichaEpi
                if (isset($dados['nada_consta_ficha_epi'])) {
                    foreach ($dados['nada_consta_ficha_epi'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->NadaConstaFichaEpi()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'NadaConstaFichaEpi',
                                'label' => 'NADA CONSTA DE BAIXA E FICHA DE ENTREGA DE EPI'
                            ]);
                        }
                    }
                }

                //Remove ComprovanteDevolucaoCtps
                if (isset($dados['comprovante_devolucao_ctpsDel'])) {
                    foreach ($dados['comprovante_devolucao_ctpsDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ComprovanteDevolucaoCtps
                if (isset($dados['comprovante_devolucao_ctps'])) {
                    foreach ($dados['comprovante_devolucao_ctps'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ComprovanteDevolucaoCtps()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ComprovanteDevolucaoCtps',
                                'label' => 'COMPROVANTE DEVOLUÇÃO CTPS ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove PppAssinado
                if (isset($dados['ppp_assinadoDel'])) {
                    foreach ($dados['ppp_assinadoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova PppAssinado
                if (isset($dados['ppp_assinado'])) {
                    foreach ($dados['ppp_assinado'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->PppAssinado()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'PppAssinado',
                                'label' => 'PPP ASSINADO'
                            ]);
                        }
                    }
                }

                //Remove ArquivamentoEletronico
                if (isset($dados['arquivamento_eletronicoDel'])) {
                    foreach ($dados['arquivamento_eletronicoDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ArquivamentoEletronico
                if (isset($dados['arquivamento_eletronico'])) {
                    foreach ($dados['arquivamento_eletronico'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ArquivamentoEletronico()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ArquivamentoEletronico',
                                'label' => 'FORMA DE ARQUIVAMENTO REGISTRO ELETRÔNICO'
                            ]);
                        }
                    }
                }

                //Remove ArquivamentoDossie
                if (isset($dados['arquivamento_dossieDel'])) {
                    foreach ($dados['arquivamento_dossieDel'] as $id_anexo) {
                        $arquivo = Arquivo::find($id_anexo);
                        $arquivo->excluir();
                    }
                }
                // inseri uma nova ArquivamentoDossie
                if (isset($dados['arquivamento_dossie'])) {
                    foreach ($dados['arquivamento_dossie'] as $index => $anexo) {
                        $arquivo = Arquivo::whereChave($anexo['chave'])->whereId($anexo['id'])->first();
                        if ($arquivo) {
                            $arquivo->temporario = false;
                            $arquivo->chave = '';
                            $arquivo->save();
                            $feedback->ArquivamentoDossie()->attach($arquivo->id, [
                                'curriculo_id' => $feedback->curriculo_id,
                                'tipo' => 'ArquivamentoDossie',
                                'label' => 'FORMA DE ARQUIVAMENTO DOSSIÊ'
                            ]);
                        }
                    }
                }

                DB::commit();
                return response()->json([], 201);
            } catch (\Exception $e) {
                DB::rollback();
                $msg = "error EM SALVAR DOSSIE:  {$e->getMessage()} , {$e->getCode()}, {$e->getLine()} | Usuario: " . User::find(auth()->id())->nome;
                \Log::debug($msg);
                return response()->json(['msg' => $msg], 400);
            }
        }
    }

    // Anexos-------------------------------------------------
    public function uploadAnexos(Request $request)
    {
        //dd($request->input());
        if ($request->file('arquivo')->isValid()) {
            $mimeType = $request->file('arquivo')->getMimeType();
            $permitidos = [
                Arquivo::MIME_JPEG,
                Arquivo::MIME_PNG,
                Arquivo::MIME_PDF,
                Arquivo::MIME_JPG,
                Arquivo::MIME_GIF,
            ];
            if (in_array($mimeType, $permitidos)) {
                $arquivo = Arquivo::gravaArquivo($request, 'arquivo', 'disco-dossie');
                return response()->json($arquivo, 201);
            } else {
                return response()->json([
                    'msg' => "O upload do arquivo \"{$request->file('arquivo')->getClientOriginalName()}\" falhou. Permitidos apenas imagens JPG/JPEG ou PDF.",
                    'erros' => []
                ], 400);
            }
        } else {
            return response()->json([
                'msg' => "O upload do anexo falhou",
                'erros' => []
            ], 400);
        }


    }

    public function anexoShow(Request $request, $arquivo)
    {
        $path = Arquivo::buscaPath($arquivo);
        if ($path == false) {
            return response("", 404);
        } else {
            $conteudo = Arquivo::buscaConteudo($arquivo);
            header("Content-type: " . Arquivo::getMimeType($path));
            header('Content-Length: ' . filesize($path));
            echo $conteudo;
        }
    }

    public function anexoDelete(Request $request, $arquivo)
    {
        //Se esta apagando realmente um anexo_imovel
        $disco = Arquivo::nomeDisco($arquivo);
        $permitidos = [
            Arquivo::DISCO_DOSSIE
        ];
        if (in_array($disco, $permitidos) == false) {
            return response("", 404);
        }
        //Apagar
        $model = Arquivo::findByArquivo($arquivo);
        if ($model && $model->temporario) {
            Arquivo::apagar($arquivo);
            return response("", 200);

        } else {
            return response("Não foi possível apagar o anexo", 400);
        }

    }

    //anexo ou foto
    public function download(Request $request, $arquivo)
    {
        //Fazer a validacao (middleware) de download para anexos-cliente , anexos-ocorrencias, aqui se nescessario...
        $disco = Arquivo::nomeDisco($arquivo);
        $permitidos = [
            Arquivo::DISCO_DOSSIE
        ];
        if (in_array($disco, $permitidos) == false) {
            return response("", 404);
        }

        $url = Arquivo::buscaPath($arquivo);
        if ($url) {
            $model = Arquivo::findByArquivo($arquivo);
            return response()->download($url, $model->nome . $model->extensao);
        } else {
            return response("", 404);
        }
    }
}
